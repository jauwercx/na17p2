# Modèle Logique des Données _ relationnel


## TABLES:

**Karateka** (#id: posint, nom: VARCHAR, prenom: VARCHAR, poids: posint, taille: posint, dateNais: Date, photo : VARCHAR, ceinture: String [Jaune,Orange,Verte,Bleue,Marron,Noire], nb_dans : posint)

DF : 
- id -> nom, prenom, poids, taille, dateNais, photo, ceinture, nb_dans

BCNF, hormis la clé primaire, aucune autre clé ne permet de déterminer un autre attribut de la même relation.

**Dirigeant**(#id: posint, nom: VARCHAR, prenom: VARCHAR, email: VARCHAR, telephone: VARCHAR) avec email et telephone unique et email like '%@%'

DF : 
- id -> nom, prenom, email, email, telephone
- email -> nom, prenom, telephone, id
- telephone -> nom, prenom, email, id

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Club**(#nom: VARCHAR, site_web: VARCHAR, id_dirigeant -> Dirigeant.id)

DF : 
- nom -> site_web, id_dirigeant

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.


**Professeur**(#id: posint, nom: VARCHAR, prenom: VARCHAR, dateNais: Date, ceinture: VARCHAR [Jaune,Orange,Verte,Bleue,Marron,Noire], nb_dans : posint)

DF : 
- id -> nom, prenom, dateNais, ceinture, nb_dans

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Mouvement**(#nom_japonais: VARCHAR, traduction_francaise: VARCHAR, schema: VARCHAR, categorie VARCHAR [déplacement,défense,techniques_de_bras,techniques_de_jambes], sous-catégorie: VARCHAR [poings,coudes,pied,genoux,positions,projections,clés]) avec traduction_francaise unique

DF : 
- nom_japonais -> traduction francaise, schema, categorie, sous-categorie
- traduction_francaise -> nom_japonais, schema, categorie, sous-categorie

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Katas**(#nom_japonais: VARCHAR, traduction_francaise: VARCHAR, description: VARCHAR, ceinture: VARCHAR [Jaune,Orange,Verte,Bleue,Marron,Noire], nb_dans: posint, schema: VARCHAR, famille_kata -> Famille_Katas.nom_japonais, videos -> Video_Kata.video)

DF : 
- nom_japonais -> traduction_francaise, decription, ceinture, nb_dans, schema, famille_kata, videos
- traduction_franaise -> nom_japonais, decription, ceinture, nb_dans, schema, famille_kata, videos

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Famille_katas**(#nom_japonais: VARCHAR, traduction_francaise: VARCHAR) avec traduction_francaise unique

DF:
- nom_japonais -> traduction_franaise
- traduction_franaise -> nom_japonais

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Video_kata**(#video: VARCHAR, #nom_kata -> katas.nom_japonais)

DF : BCNF car plusieurs vidéos peuvent décrire un kata, et un kata peut avoir plusieurs vidéos.

**Point_type_coup**(#id: posint, nom -> Mouvement.sous-categorie, nb_point : posint)

DF : 
- id -> nom, nb_point

BCNF, hormis la clé, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Competition_kumite**(#id: posint, nom: VARCHAR, date: Date, lieu: VARCHAR, photo:VARCHAR, site_web: VARCHAR, mail: VARCHAR, club: VARCHAR) with mail like '%@%'

DF : id -> nom, date, lieu, photo, site_web, mail, club

BCNF, hormis la clé, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.


**Confrontation_kumite**(#id: posint, participant1 -> Karateka.id, participant2 => Karateka.id, date: date, score_karat1: posint, score_karat2: posint compet_kumite -> Competition_kumite.id, compet_mixte -> Competition_mixte.id) avec ((participant1 != participant2) et ((compet_mixte is not null and compet_kumite is null) or (compet_mixte is null and compet_kumite is not null))

DF : id -> particpant1, participant2, date, score_karat1, score_karat2, compet_kumite, compet_mixte

BCNF, hormis la clé, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Competition_tameshi_wari**(#id: posint, nom: VARCHAR, date: Date, lieu: VARCHAR, photo:VARCHAR, site_web: VARCHAR, mail: VARCHAR, club:VARCHAR) with mail like %@%

DF : id -> nom, date, lieu, photo, site_web, mail, club

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Confrontation_tameshi_wari**(#id: posint, gagnant -> Karateka.id, perdant => Karateka.id, date: date, score_karat1: posint, score_karat2: posint, compet_tameshi_wari -> Competition_tameshi_wari.id, compet_mixte -> Competition_mixte.id) avec ((perdant != gagnant) et ((compet_mixte is not null and compet_kumite is null) or (compet_mixte is null and compet_kumite is not null))

DF : id -> gagnant, perdant, date, score_karat1, score_karat2, compet_tameshi_wari, compet_mixte

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Competition_kata**(#id: posint, nom: VARCHAR, date: Date, lieu: VARCHAR, photo:VARCHAR, site_web: VARCHAR, mail: VARCHAR, club: VARCHAR) with mail like '%@%'

DF : id -> nom, date, lieu, photo, site_web, mail, club

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Confrontation_kata**(#id: posint, gagnant -> Karateka.id, perdant => Karateka.id, date: date, score_karat1: posint, score_karat2: posint, compet_kata -> Competition_kata.id, compet_mixte -> Competition_mixte.id, nom_du_kata -> Katas.nom_japonais) avec ((perdant != gagnant) et ((compet_mixte is not null and compet_kata is null) or (compet_mixte is null and compet_kata is not null))

DF : id -> gagnant, perdant, date, score_karat1, score_karat2, compet_kata, compet_mixte

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Competition_mixte**(#id: posint, nom: VARCHAR, date: Date, lieu: VARCHAR, photo:VARCHAR, site_web: VARCHAR, mail: VARCHAR, club: VARCHAR) with mail like '%@%'

DF : id -> nom, date, lieu, photo, site_web, mail, club

BCNF, hormis les clés, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Composition_kata**(#nom_kata -> Katas.nom_japonais, nom_mouvement -> Mouvement.nom_japonais, #position: posint)

DF : #nom_kata, #position -> nom_mouvement

BCNF, hormis la clé, aucune autre attribut ne permet de déterminer un autre attribut de la même relation.

**Maitriser_kata**(#nom_kata -> Katas.nom_japonais, #karateka -> karateka.id)

BCNF, un karateka peut maitriser plusieurs katas, et un kata peut être maitrisé par plusieurs karatekas

**Appartenir_club**(#nom_club -> Club.nom, #professeur -> Professeur.id)

DF : BCNF, un club peut posèder plusieurs professeur, et un professeur peut  enseigner dans plusieurs clubs

# CONTRAINTES

- Tous les attributs sont NOT NULL, sauf ceux concernés par des contraintes de type 'XOR'
- Les champs (id) sont supérieur à 0
- Karatekas(nb_dans) && Professeur(nb_dans) >= 0 et <= 10
- Un kata a au moins un mouvement
- Un kata a au moins une vidéo de présentation
- Une video peut présenter plusieurs katas
- Un kata peut possèder plusieurs vidéo
- Un professeur peut enseigner dans plusieurs clubs
