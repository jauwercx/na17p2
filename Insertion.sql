-- Script insertion de données de base



INSERT INTO Karateka(id, nom, prenom, poids, taille, dateNais, photo, ceinture, nb_dans)
VALUES (1, 'Stark', 'Tony', 45, 188,  '1995-01-03', 'URLphotoStark', 'Jaune', 0),
       (2, 'Potts', 'Pepper', 84, 185,  '1991-07-29', 'URLphotoPotts', 'Bleue', 0),
       (3, 'Parker', 'Peter', 96, 191, '1988-07-28', 'URLphotoParker', 'Noire', 4),
       (4, 'Parker', 'May', 63, 166, '1980-02-28', 'URLphotoParkerMay', 'Jaune', 0),
       (5, 'Hogan', 'Happy', 96, 191, '1983-04-17', 'URLphotoHogan', 'Orange', 0),
       (6, 'Osborn', 'Norman', 96, 191, '1991-11-30', 'URLphotoOsborn', 'Orange', 0),
       (7, 'Khan', 'Kamala', 96, 191, '1990-03-04', 'URLphotoKhan', 'Noire', 4),
       (8, 'Amaquelin', 'Medusalith', 96, 191, '1988-01-13', 'URLphotoAmaquelin', 'Marron', 0),
       (9, 'Laufey', 'Loki', 96, 191, '1990-07-25', 'URLphotoLaufey', 'Noire', 0),
       (10, 'Ross', 'Thunderbolt', 96, 191, '1996-07-26', 'URLphotoRoss', 'Noire', 1),
       (11, 'Rogers', 'Steve', 96, 191, '1984-05-15', 'URLphotoRogers', 'Orange', 0),
       (12, 'Barnes', 'Bucky', 96, 191, '1989-02-02', 'URLphotoBarnes', 'Noire', 4),
       (13, 'Romanov', 'Natasha', 96, 191, '1988-07-14', 'URLphotoRomanov', 'Bleue', 0),
       (14, 'Masters', 'Anthony', 96, 191, '1994-09-30', 'URLphotoMasters', 'Marron', 0),
       (15, 'Zemo', 'Baron', 96, 191, '1991-02-28', 'URLphotoZemo', 'Jaune', 0);

INSERT INTO Dirigeant(id, nom, prenom, email, telephone)
VALUES (1, 'Johnson', 'Crystal', 'Johnson.Crystal@hotmail.fr', '805-478-9964'),
       (2, 'Topps', 'Louise','Topps.Louise@hotmail.fr', '763-543-8547'),
       (3, 'Cothran', 'Greg','Cothran.Greg@hotmail.fr', '989-218-4214'),
       (4, 'Voyles', 'Erin','Voyles.Erin@hotmail.fr', '319-445-1880'),
       (5, 'Anne', 'Ponce','Anne.Ponce@hotmail.fr', '856-528-1002'),
       (6, 'Parks', 'Christopher','Parks.Christopher@hotmail.fr', '580-369-2078');

INSERT INTO Club(nom, site_web, id_dirigeant)
VALUES ('les Bisons Sauvages', 'http://BisonsSauvages.fr', 3),
       ('les Griffons Noirs', 'http://GriffonsNoirs.fr', 2),
       ('les Belettes Bleues', 'http://BelettesBleues.fr', 1),
       ('les Yétis Austères', 'http://YétisAustères.fr', 6),
       ('les Crocodiles Farouches', 'http://CrocodilesFarouches.fr', 4),
       ('les Lézards Sinistres', 'http://LézardsSinistres.fr', 5),
       ('les Trolls Brillants', 'http://TrollsBrillants.fr', 5),
       ('les Hyènes Glorieuses', 'http://HyènesGlorieuses.fr', 2);


INSERT INTO Professeur(id, nom, prenom, dateNais, ceinture, nb_dans)
VALUES (1, 'Henry', 'Zimmerman', '1981-07-03', 'Jaune', 0),
       (2, 'Andrew', 'Henry',  '1984-01-29', 'Bleue', 0),
       (3, 'Ruth', 'Farris', '1988-03-28', 'Noire', 4),
       (4, 'Carol', 'Dunn','1973-05-31', 'Jaune', 0),
       (5, 'Lindsay', 'Tatum', '1972-06-17', 'Orange', 0),
       (6, 'Christina', 'Thompson','1978-11-30', 'Orange', 0),
       (7, 'Edith', 'Carrozza', '1979-12-04', 'Noire', 4),
       (8, 'Clarence', 'Culligan', '1982-08-13', 'Marron', 0),
       (9, 'Shirley', 'Miller', '1981-07-25', 'Noire', 0),
       (10, 'Helen', 'Thomas', '1980-01-26', 'Noire', 1);


INSERT INTO Mouvement(nom_japonais,traduction_francaise, schema, categorie, sous_categorie)
VALUES ('Zenkutsu dachi','Position vers l avant' ,'Zenkutsudachi.png' , 'déplacement', 'positions'),
       ('Kiba Dachi','Pose du cheval','KibaDachi.png', 'defense','positions'),
       ('Fudo Dachi','inderacinable','FudoDachi.png', 'techniques_de_bras','positions' ),
       ('Hachiji Dachi','position naturelle et preparatoire','HachijiDach.png', 'techniques_de_jambes','positions'),
       ('Heiko Dachi','Position d attente équilibree' ,'HeikoDachi', 'déplacement','positions'),
       ('Sochin Dachi','inderacinable2','SochinDachi.png', 'techniques_de_jambes','positions'),
       ('Musubi Dachi','position d attente talons  joints','MusubiDachi.png','techniques_de_jambes','positions'),
       ('Gedan barai','traduction Gedan barai','Gedanbarai.png','techniques_de_bras' ,'positions'),
       ('Shuto Uke','traduction Shuto Uke','ShutoUke.png', 'techniques_de_bras','positions'),
       ('Teisho Uke','traduction Teisho Uke','TeishoUke.png', 'techniques_de_bras','positions'),
       ('Jugi Uke','traduction Jugi Uke','JugiUke.png', 'techniques_de_jambes','pied'),
       ('Tate Uke','traduction Tate Uke','TateUke.png', 'defense','pied'),
       ('Tobi Mae Geri','traduction Tobi Mae Geri','TobiMaeGeri.png', 'techniques_de_jambes','positions'),
       ('Tobi Yoko Geri','traduction Tobi Yoko Geri','TobiYokoGeri.png', 'techniques_de_jambes','pied'),
       ('UshiroGeriKeage','traduction UshiroGeriKeage','UshiroGeriKeage.png', 'techniques_de_jambes','positions'),
       ('MikazukiGeri','traduction MikazukiGeri','MikazukiGeri.png', 'techniques_de_jambes','pied'),
       ('Hitsui Geri','traduction Hitsui Geri','Hitsui Geri.png', 'defense','pied'),
       ('Fumizuki','traduction Fumizuki','Fumizuki.png', 'defense','pied'),
       ('Heiko-zuki','traduction Heiko-zuki','Heiko-zuki.png', 'defense','pied'),
       ('Kagi-zuki','traduction Kagi-zuki','Kagi-zuki.png', 'defense','pied'),
       ('Hasami zuki','traduction Hasami zuki','Hasami uki.png', 'defense','pied'),
       ('Mawashi zuki','traduction Mawashi zuki','Mawashizuki.png', 'defense','pied'),
       ('Morote-zuki','coup de poing double','Morote-zuki.png', 'techniques_de_bras','poings');


INSERT INTO Famille_katas(nom_japonais, traduction_francaise)
VALUES ('Heian','esprit en paix' ),
       ('Tekki' , 'cheval de fer'),
       ('Bassai','Pour prendre d assaut une forteresse'),
       ('Kanku', 'pour regarder le ciel '),
       ('Enpi','hirondelle volante'),
       ('Jion','amour et bonte');

INSERT INTO Katas(nom_japonais,traduction_francaise,description,ceinture,nb_dans,schema,famille_kata)
VALUES ('Heian Shodan','esprit en paix niveau 1','Niveau 1 du kata esprit en paix','Jaune',0,'HeianShodan.png', 'Heian'),
       ('Heian Nidan', 'esprit en paix niveau 2','Niveau 2 du kata esprit en paix','Jaune',0,'HeianNidan.png', 'Heian'),
       ('Heian Sandan', 'esprit en paix niveau 3','Niveau 3 du kata esprit en paix','Orange',0,'HeianSandan.png', 'Heian'),
       ('Heian Yondan', 'esprit en paix niveau 4','Niveau 4 du kata esprit en paix','Orange',0,'HeianYondan.png', 'Heian'),
       ('Heian Godan', 'esprit en paix niveau 5','Niveau 5 du kata esprit en paix','Verte',0,'HeianGodan.png', 'Heian'),
       ('Tekki Shodan' , 'cheval de fer niveau 1','Niveau 1 du kata cheval de fer', 'Jaune',0,'TekkiShodan.png','Tekki'),
       ('Tekki Nidan', 'cheval de fer niveau 2', 'Niveau 2 du kata cheval de fer', 'Verte',0,'TekkiNidan.png','Tekki'),
       ('Tekki Sandan', 'cheval de fer niveau 3', 'Niveau 3 du kata cheval de fer', 'Bleue',0,'TekkiSandan.png','Tekki'),
       ('Bassai Dai', 'pour prendre d’assaut une forteresse, Big ', 'Niveau 1 de : pour prendre d’assaut une forteresse', 'Bleue',0,'BassaiDai.png','Bassai'),
       ('Bassai Sho' , 'pour prendre d’assaut une forteresse, Small','Niveau 2 de : pour prendre d’assaut une forteresse', 'Noire',4,'BassaiSho.png','Bassai'),
       ('Kanku Dai' , 'pour regarder le ciel, Big ','Niveau 1 de : pour regarder le ciel', 'Marron',0,'KankuDai.png','Kanku'),
       ('Kanku Sho' ,'pour regarder le ciel, Small','Niveau 2 de : pour regarder le ciel', 'Noire',2,'KankuSho.png','Kanku'),
       ('Enpi','Hirondelle volante','Description de l Hirondelle Volante','Jaune',0,'Enpi.png','Enpi'),
       ('Jion','Amour et bonte','Description du kata : amour et bonte','Marron',0,'Jion.png','Jion');

INSERT INTO Video_kata(video,nom_kata)
VALUES ('HeianShodan.mp4','Heian Shodan'),
       ('HeianNidan.mp4', 'Heian Nidan'),
       ('HeianSandan.mp4','Heian Sandan'),
       ('HeianYondan.mp4','Heian Yondan'),
       ('HeianGodan.mp4', 'Heian Godan'),
       ('TekkiShodan.mp4', 'Tekki Shodan'),
       ('TekkiNidan.mp4', 'Tekki Nidan'),
       ('TekkiSandan.mp4', 'Tekki Sandan'),
       ('BassaiDai.mp4', 'Bassai Dai'),
       ('BassaiSho.mp4', 'Bassai Sho'),
       ('KankuDai.mp4', 'Kanku Dai'),
       ('KankuSho.mp4', 'Kanku Sho'),
       ('Enpi.mp4', 'Enpi'),
       ('Jion.mp4', 'Jion');


INSERT INTO Point_type_coup(id,nom,nb_point)
VALUES (1,'Morote-zuki',3),
       (2,'Hitsui Geri',2),
       (3,'Hasami zuki',2),
       (4,'Kagi-zuki',2),
       (5,'Jugi Uke',2);


INSERT INTO Competition_mixte(id,nom,date,lieu,photo,site_web,mail,club)
VALUES (1,'Tournoi Qualification Olympique','2020-05-12','Paris','TournoiQualifiactionOlympique.png','http://TournoiQualifiactionOlympique.fr','TournoiQualifiactionOlympique@hotmail.fr','les Bisons Sauvages');

INSERT INTO Competition_kumite(id,nom,date,lieu,photo,site_web,mail,club)
VALUES (1,'Tournoi National de Kumite','2020-06-26','Lille','TournoiNationalKumite.png','http://TournoiNationalKumite.fr','TournoiNationalKumite@hotmail.fr','les Belettes Bleues');

INSERT INTO Confrontation_kumite(id,participant1,participant2,date,score_karat1,score_karat2,compet_kumite, compet_mixte)
VALUES (1,1,2,'2019-05-23',12,14,1,null),
       (2,11,1,'2019-08-14',13,4,1,null),
       (3,4,3,'2019-06-03',9,17,1,null),
       (4,8,9,'2019-04-01',19,18,null,1);

INSERT INTO Competition_tameshi_wari(id,nom,date,lieu,photo,site_web,mail,club)
VALUES (1,'Tournoi National de Tameshi_Wari','2020-06-29','Lyon','TournoiNationalTameshiWari.png','http://TournoiNationalTameshiWari.fr','TournoiNationalTameshiWari@hotmail.fr','les Hyènes Glorieuses');

INSERT INTO Confrontation_tameshi_wari(id,gagnant,perdant,date,score_karat1,score_karat2,compet_tameshi_wari, compet_mixte)
VALUES (1,1,2,'2019-12-24',21,14,1,null),
       (2,4,3,'2019-12-13',10,4,1,null),
       (3,6,5,'2020-01-03',17,8,1,null),
       (4,7,10,'2019-01-09',19,18,null,1);

INSERT INTO Competition_kata(id,nom,date,lieu,photo,site_web,mail,club)
VALUES (1,'Tournoi National de Kata','2020-04-01','Lyon','TournoiNationalKata.png','http://TournoiNationalKata.fr','TournoiNationalKata@hotmail.fr','les Trolls Brillants');

INSERT INTO Confrontation_kata(id,gagnant,perdant,date,score_karat1,score_karat2,compet_kata, compet_mixte)
VALUES (1,12,13,'2019-11-17',14,12,1,null),
       (2,15,14,'2019-04-11',10,3,1,null),
       (3,10,12,'2020-02-20',19,11,1,null),
       (4,4,5,'2019-12-25',5,2,null,1);

INSERT INTO Composition_kata(nom_kata, nom_mouvement, position)
VALUES ('Heian Shodan', 'Zenkutsu dachi', 1),
	('Heian Shodan', 'Tobi Yoko Geri',2),
	('Heian Shodan', 'Zenkutsu dachi', 3),
	('Bassai Dai', 'Heiko Dachi',1),
	('Bassai Dai','Morote-zuki',2);

INSERT INTO Maitriser_kata (nom_kata, karateka)
VALUES ('Heian Nidan',1),
	('Heian Sandan',1),
	('Tekki Shodan',2),
	('Bassai Dai',3),
	('Tekki Sandan',4),
	('Tekki Sandan',5),
	('Bassai Dai',5),
	('Enpi',5),
	('Enpi',6),
	('Enpi',7),
	('Enpi',8),
	('Tekki Shodan',6),
	('Tekki Shodan',7);

INSERT INTO Appartenir_club (nom_club, professeur)
VALUES ('les Griffons Noirs',1),
	('les Griffons Noirs',2),
	('les Griffons Noirs',3),
	('les Yétis Austères',1),
	('les Crocodiles Farouches',4),
	('les Crocodiles Farouches',5),
	('les Bisons Sauvages',8),
	('les Bisons Sauvages',10),
	('les Hyènes Glorieuses',6),
	('les Trolls Brillants',7),
	('les Trolls Brillants',8),
	('les Lézards Sinistres',1),
	('les Griffons Noirs',10),
	('les Belettes Bleues',9),
	('les Lézards Sinistres',9);
	


