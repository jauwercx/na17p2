-- Pour chaque katas, affichage de ses mouvements et de leurs positions
SELECT nom_kata, nom_mouvement, schema, position FROM Composition_kata, Katas
WHERE  Composition_kata.nom_kata=Katas.nom_japonais
ORDER BY nom_kata,position;

-- Pour chaque karateka, affichage de ses informations et des katas qu'il maitrise
SELECT nom, prenom, poids, taille, dateNais, photo, Karateka.ceinture, nb_dans, nom_kata FROM Karateka, Maitriser_kata
WHERE  Maitriser_kata.karateka=Karateka.id
ORDER BY nom;


-- Pour chaque club, affichage des informations de ses professeurs
SELECT club.nom, professeur.nom, prenom, dateNais, ceinture, nb_dans FROM Professeur, Club, Appartenir_club
WHERE Appartenir_club.nom_club=Club.nom AND Appartenir_club.professeur=Professeur.id
ORDER BY club.nom;


-- Selection des informations du dirigeant du club pour chaque club
SELECT dirigeant.nom, prenom, email, telephone, club.nom FROM dirigeant, club
WHERE dirigeant.id=club.id_dirigeant
ORDER BY dirigeant.nom;


--Selection des quatres types de de compétitions
CREATE OR REPLACE VIEW v_competition AS 
    SELECT id, nom, date, lieu, photo, site_web, mail, club, 'kata' AS type FROM competition_kumite
    UNION
    SELECT id, nom, date, lieu, photo, site_web, mail, club, 'kumite' AS type FROM competition_kata
    UNION
    SELECT id, nom, date, lieu, photo, site_web, mail, club, 'tameshi_wari' AS type FROM competition_tameshi_wari
    UNION
    SELECT id, nom, date, lieu, photo, site_web, mail, club, 'mixte' AS type FROM competition_mixte;


-- Selection d'informations sur certains type de competition
SELECT id, nom, date, lieu from v_competition WHERE type = 'mixte' OR type = 'kata';

--Récupérer toutes les compétitions qui ont lieu à Lyon
SELECT * FROM v_competition WHERE lieu='Lyon';


-- Selection des compétitions organisées par chaque club
SELECT id, competition.nom, date, lieu, photo, competition.site_web, mail, club
FROM v_competition AS competition
ORDER BY club;


-- Selection des schema, videos et description de chaque kata
SELECT nom_japonais, traduction_francaise, description, schema, video
FROM Katas, Video_kata
WHERE Video_kata.nom_kata=Katas.nom_japonais
ORDER BY nom_japonais;