# OBJETS
---
Karatekas

 - Nom
 - Prenom
 - Poids (kg)
 - Taille (cm)
 - DateNais
 - Photo
 - Ceinture (Jaune, Orange, Verte, Bleue, Marron, Noire) 
 - Nb_dans
 -  Historique des confrontations
 - Liste Katas Maitrisés

Competition

- Nom
- Date
 - Lieu
 - Liste des participants (>= 2)
 - Liste des confrontations (>=1)
 - Classement final
 - Photo
 - Site web
 - Mail
 - Type de compétition (Kumite/Katas/Tameshi Wari/Mixte)


Si Competition de Kumite ou Competition Mixte :
 - Liste des coups interdits (Aucun ou plusieurs)
 - Nb points / type de coup

Nombre de points par type de coup
- Nom du coup
 - Nombre de points que le coup accorde au karateka


Confrontation
 - 	Score_Vainqueur
 - 	Score_Perdant
 - Date


Si confrontation Kata
 - Nom du Kata
 
Si Confrontation Kumite
 - Liste des coups ayant marqués des points pour le karateka1
 - Liste des coups ayant marqués des points pour le karateka2

Katas
 - Nom japonais
 - Traduction française
 - Description
 - 	Une ou plusieurs vidéos
 - Un schéma
 - Liste de mouvements (>=1)
 - Nombre de mouvements
 - Ceinture
     - Jaune à noire
 - Nombre de dans
     - Entre 0 et 10
 - Famille de kata

Famille de kata
 - Nom japonais
 - Traduction française
	
Mouvements
 - Nom japonais
 - Traduction française
 - Schéma
 - Catégories
     - (Déplacement, défense, techniques de bras, techniques de jambes)
 - Sous-catégorie
     - (Poings, coudes, pied, genoux, positions, projections, clés)

Club
 - Nom
 - Site web
 - Nom dirigeant
 - Listes professeurs
 - Liste Karatekas
 - Liste compétitions organisés
	
Professeur
 - Nom
 - Prenom
 - DateNais
 - Ceinture
 - Nombre de dans
 - Club
	
Dirigeant
 - Nom
 - Prenom
 - Email
 - Telephone 
 - Liste clubs possédés (> 0)


# HYPOTHESES
---

- Deux karatekas, professeurs ou dirigeants peuvent avoir les mêmes nom et prenom et date de naissance
-  La taille d'un karateka s'exprime en cm
-  Odre de couleur des ceintures : Jaune, Orange, Verte, Bleue, Marron, Noire
- Le nombre de dans d'un karatekas est de 0 tant que sa ceinture n'est pas noire
- L'âge d'un karateka est calculé à partir de sa date de naissance
- Les images, vidéos et photos sont représentés comme des chaînes de caractère (URL de la ressource)
- 2 compétions ne peuvent pas avoir le même nom à la même date
- Le nombre de participant à une compétition est une puissance de 2 >= 2
- Lieu d'une compétition : uniquement le nom de la ville (le site Web de la compétition pourrais contenir les informations additionelles)
- Chaque confrontation possède une date afin de l'identifier de manière unique (avec le nom des deux participants)
- Lors d'une confrontation de kumite, les scores peuvent être calculés à l'aide de la liste des coups et du nombre de points par coup
- Une confrontation oppose deux karatekas différents
- Deux famille de kata ne peuvent pas avoir le même nom
- Deux club ne peuvent pas avoir le même nom
- Une vidéo peut illustrer plusieurs katas, et un katas peut avoir plusieurs vidéos
- Un club peut organiser plusieurs compétitions
- Un dirigeant peut possèder plusieurs clubs

# UTILISATEURS
---
Un seul rôle qui correspond à l'administrateur de la base de données


# FONCTIONS
---

-  L'utilisateur peut initialiser la base avec tous les katas, mouvements et grades du karaté
-  Créer des karatékas, des clubs et des compétitions 
-  Inscrire des karatékas dans des clubs et à des compétitions
-  Saisir le résultat des confrontations au fur et à mesure
-  Obtenir la liste de toutes confrontations et résultats d'une compétition
- Obtenir le descriptif complet d'un karatéka (avec toutes ses compétitions et confrontations)

