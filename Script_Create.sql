DROP VIEW IF EXISTS v_competition;

DROP TABLE IF EXISTS Maitriser_kata ;
DROP TABLE IF EXISTS Composition_kata;
DROP TABLE IF EXISTS Confrontation_kata;
DROP TABLE IF EXISTS Competition_kata;
DROP TABLE IF EXISTS Confrontation_tameshi_wari;
DROP TABLE IF EXISTS Competition_tameshi_wari;
DROP TABLE IF EXISTS Confrontation_kumite;
DROP TABLE IF EXISTS Competition_kumite;
DROP TABLE IF EXISTS Competition_mixte;
DROP TABLE IF EXISTS Point_type_coup;
DROP TABLE IF EXISTS Video_kata;
DROP TABLE IF EXISTS Katas;
DROP TABLE IF EXISTS Famille_katas;
DROP TABLE IF EXISTS Mouvement;
DROP TABLE IF EXISTS Appartenir_club;
DROP TABLE IF EXISTS Professeur;
DROP TABLE IF EXISTS Club;
DROP TABLE IF EXISTS Dirigeant;
DROP TABLE IF EXISTS Karateka;

CREATE TABLE Karateka (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom       VARCHAR NOT NULL,
    prenom    VARCHAR NOT NULL,
    poids INTEGER NOT NULL CHECK (poids>0),
    taille INTEGER NOT NULL CHECK ( taille>0),
    dateNais DATE NOT NULL,
    photo VARCHAR NOT NULL,
    ceinture VARCHAR NOT NULL CHECK (Ceinture='Jaune' OR Ceinture='Orange' OR Ceinture='Verte' OR Ceinture='Bleue' OR Ceinture='Marron' OR Ceinture='Noire'),
    nb_dans INTEGER NOT NULL CHECK (nb_dans >= 0 AND nb_dans <= 10),
    PRIMARY KEY (id)
);

CREATE TABLE Dirigeant (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom       VARCHAR NOT NULL,
    prenom    VARCHAR NOT NULL,
    email VARCHAR NOT NULL UNIQUE CHECK (email LIKE '%@%'),
    telephone VARCHAR NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE Club (
    nom       VARCHAR NOT NULL,
    site_web VARCHAR NOT NULL,
    id_dirigeant INTEGER,
    FOREIGN KEY (id_dirigeant) REFERENCES Dirigeant(id),
    PRIMARY KEY (nom)
);

CREATE TABLE Professeur (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom       VARCHAR NOT NULL,
    prenom    VARCHAR NOT NULL,
    dateNais DATE NOT NULL,
    ceinture VARCHAR NOT NULL CHECK (Ceinture='Jaune' OR Ceinture='Orange' OR Ceinture='Verte' OR Ceinture='Bleue' OR Ceinture='Marron' OR Ceinture='Noire'),
    nb_dans INTEGER NOT NULL CHECK (nb_dans >= 0 AND nb_dans <= 10),
    PRIMARY KEY (id)
);

CREATE TABLE Appartenir_club (
    nom_club VARCHAR NOT NULL,
    professeur INTEGER NOT NULL CHECK (professeur > 0),
    FOREIGN KEY (nom_club) REFERENCES Club(nom),
    FOREIGN KEY (professeur) REFERENCES Professeur(id),
    PRIMARY KEY (nom_club, professeur)
);

CREATE TABLE Mouvement (
    nom_japonais VARCHAR NOT NULL,
    traduction_francaise    VARCHAR NOT NULL UNIQUE,
    schema VARCHAR NOT NULL,
    categorie VARCHAR NOT NULL CHECK (categorie='déplacement' OR categorie='defense' OR categorie='techniques_de_bras' OR categorie='techniques_de_jambes'),
    sous_categorie VARCHAR NOT NULL CHECK (sous_categorie='poings' OR sous_categorie='coudes' OR sous_categorie= 'pied' OR sous_categorie= 'genoux' OR sous_categorie= 'positions' OR sous_categorie= 'projections' OR sous_categorie='clés'),
    PRIMARY KEY (nom_japonais)
);

CREATE TABLE Famille_katas (
    nom_japonais VARCHAR NOT NULL,
    traduction_francaise    VARCHAR NOT NULL UNIQUE,
    PRIMARY KEY (nom_japonais)
);


CREATE TABLE Katas (
    nom_japonais VARCHAR NOT NULL,
    traduction_francaise    VARCHAR NOT NULL UNIQUE,
    description VARCHAR NOT NULL,
    ceinture VARCHAR NOT NULL CHECK (Ceinture='Jaune' OR Ceinture='Orange' OR Ceinture='Verte' OR Ceinture='Bleue' OR Ceinture='Marron' OR Ceinture='Noire'),
    nb_dans INTEGER NOT NULL CHECK (nb_dans >= 0 AND nb_dans <= 10),
    schema VARCHAR NOT NULL,
    famille_kata VARCHAR NOT NULL,
    FOREIGN KEY (famille_kata) REFERENCES Famille_Katas(nom_japonais),
    PRIMARY KEY (nom_japonais)
);

CREATE TABLE Video_kata (
    video VARCHAR NOT NULL,
    nom_kata VARCHAR NOT NULL,
    FOREIGN KEY (nom_kata) REFERENCES Katas(nom_japonais),
    PRIMARY KEY (video, nom_kata)
);

CREATE TABLE Point_type_coup (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom VARCHAR NOT NULL,
    nb_point INTEGER CHECK (nb_point>0),
    FOREIGN KEY (nom) REFERENCES Mouvement(nom_japonais),
    PRIMARY KEY (id)
);


CREATE TABLE Competition_mixte (
    id INTEGER NOT NULL CHECK (id > 0),
    nom VARCHAR NOT NULL,
    date DATE NOT NULL,
    lieu VARCHAR NOT NULL,
    photo VARCHAR NOT NULL,
    site_web VARCHAR NOT NULL,
    mail VARCHAR NOT NULL UNIQUE CHECK(mail LIKE '%@%'),
    club VARCHAR NOT NULL,
    FOREIGN KEY (club) REFERENCES Club(nom),
    PRIMARY KEY (id)
);

CREATE TABLE Competition_kumite (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom VARCHAR NOT NULL,
    date DATE NOT NULL,
    lieu VARCHAR NOT NULL,
    photo VARCHAR,
    site_web VARCHAR NOT NULL,
    mail VARCHAR NOT NULL UNIQUE CHECK(mail LIKE '%@%'),
    club VARCHAR NOT NULL,
    FOREIGN KEY (club) REFERENCES Club(nom),
    PRIMARY KEY (id)
);

CREATE TABLE Confrontation_kumite (
    id        INTEGER NOT NULL CHECK (id > 0),
    participant1 INTEGER NOT NULL,
    participant2 INTEGER NOT NULL,
    date DATE NOT NULL,
    score_karat1 INTEGER NOT NULL,
    score_karat2 INTEGER NOT NULL,
    compet_kumite INTEGER,
    compet_mixte INTEGER,
    CHECK (participant1 <> participant2),
    CHECK ((compet_mixte is not null and compet_kumite is null) or (compet_mixte is null and compet_kumite is not null)),
    FOREIGN KEY (participant1) REFERENCES Karateka(id),
    FOREIGN KEY (participant2) REFERENCES Karateka(id),
    FOREIGN KEY (compet_kumite) REFERENCES Competition_kumite(id),
    FOREIGN KEY (compet_mixte) REFERENCES Competition_mixte(id),
    PRIMARY KEY (id)
);

CREATE TABLE Competition_tameshi_wari (
    id        INTEGER NOT NULL CHECK (id > 0),
    nom VARCHAR NOT NULL,
    date DATE NOT NULL,
    lieu VARCHAR NOT NULL,
    photo VARCHAR,
    site_web VARCHAR NOT NULL,
    mail VARCHAR NOT NULL UNIQUE CHECK(mail LIKE '%@%'),
    club VARCHAR NOT NULL,
    FOREIGN KEY (club) REFERENCES Club(nom),
    PRIMARY KEY (id)
);

CREATE TABLE Confrontation_tameshi_wari (
    id        INTEGER NOT NULL CHECK (id > 0),
    gagnant INTEGER NOT NULL,
    perdant INTEGER NOT NULL,
    date DATE NOT NULL,
    score_karat1 INTEGER NOT NULL,
    score_karat2 INTEGER NOT NULL,
    compet_tameshi_wari INTEGER,
    compet_mixte INTEGER,
    CHECK (gagnant <> perdant),
    CHECK ((compet_mixte is not null and compet_tameshi_wari is null) or (compet_mixte is null and compet_tameshi_wari is not null)),
    FOREIGN KEY (gagnant) REFERENCES Karateka(id),
    FOREIGN KEY (perdant) REFERENCES Karateka(id),
    FOREIGN KEY (compet_tameshi_wari) REFERENCES Competition_tameshi_wari(id),
    FOREIGN KEY (compet_mixte) REFERENCES Competition_mixte(id),
    PRIMARY KEY (id)
);

CREATE TABLE Competition_kata (
    id INTEGER NOT NULL CHECK (id > 0),
    nom VARCHAR NOT NULL,
    date DATE NOT NULL,
    lieu VARCHAR NOT NULL,
    photo VARCHAR,
    site_web VARCHAR NOT NULL,
    mail VARCHAR NOT NULL UNIQUE CHECK(mail LIKE '%@%'),
    club VARCHAR NOT NULL,
    FOREIGN KEY (club) REFERENCES Club(nom),
    PRIMARY KEY (id)
); 

CREATE TABLE Confrontation_kata (
    id        INTEGER NOT NULL CHECK (id > 0),
    gagnant INTEGER NOT NULL,
    perdant INTEGER NOT NULL,
    date DATE NOT NULL,
    score_karat1 INTEGER NOT NULL,
    score_karat2 INTEGER NOT NULL,
    compet_kata INTEGER,
    compet_mixte INTEGER,
    CHECK (gagnant <> perdant),
    CHECK ((compet_mixte is not null and compet_kata is null) or (compet_mixte is null and compet_kata is not null)),
    FOREIGN KEY (gagnant) REFERENCES Karateka(id),
    FOREIGN KEY (perdant) REFERENCES Karateka(id),
    FOREIGN KEY (compet_kata) REFERENCES Competition_kata(id),
    FOREIGN KEY (compet_mixte) REFERENCES Competition_mixte(id),
    PRIMARY KEY (id)
);


CREATE TABLE Composition_kata (
    nom_kata VARCHAR NOT NULL,
    nom_mouvement VARCHAR NOT NULL,
    position INTEGER CHECK (position > 0),
    FOREIGN KEY (nom_kata) REFERENCES Katas(nom_japonais),
    FOREIGN KEY (nom_mouvement) REFERENCES Mouvement(nom_japonais),
    PRIMARY KEY (nom_kata, position)
);

CREATE TABLE Maitriser_kata (
    nom_kata VARCHAR NOT NULL,
    karateka INTEGER NOT NULL CHECK (karateka > 0),
    FOREIGN KEY (nom_kata) REFERENCES Katas(nom_japonais),
    FOREIGN KEY (karateka) REFERENCES Karateka(id),
    PRIMARY KEY (nom_kata, karateka)
);


