# Scirpt MongoDB

use auwercx
db.Club.insert(
[
  {
    "nom": "les Bisons Sauvages",
    "site_web": "http://BisonsSauvages.fr",
    "id_dirigeant": "3"
  },
  {
    "nom": "les Griffons Noirs",
    "site_web": "http://GriffonsNoirs.fr",
    "id_dirigeant": "2"
  },
  {
    "nom": "les Belettes Bleues",
    "site_web": "http://BelettesBleues.fr",
    "id_dirigeant": "1"
  },
  {
    "nom": "les Yétis Austères",
    "site_web": "http://YétisAustères.fr",
    "id_dirigeant": "6"
  },
  {
    "nom": "les Crocodiles Farouches",
    "site_web": "http://CrocodilesFarouches.fr",
    "id_dirigeant": "4"
  },
  {
    "nom": "les Lézards Sinistres",
    "site_web": "http://LézardsSinistres.fr",
    "id_dirigeant": "5"
  },
  {
    "nom": "les Trolls Brillants",
    "site_web": "http://TrollsBrillants.fr",
    "id_dirigeant": "5"
  },
  {
    "nom": "les Hyènes Glorieuses",
    "site_web": "http://HyènesGlorieuses.fr",
    "id_dirigeant": "2"
  }
]
)

db.Dirigeant.insert(
[
  {
    "id": "1",
    "nom": "Johnson",
    "prenom": "Crystal",
    "email": "Johnson.Crystal@hotmail.fr",
    "telephone": "805-478-9964"
  },
  {
    "id": "2",
    "nom": "Topps",
    "prenom": "Louise",
    "email": "Topps.Louise@hotmail.fr",
    "telephone": "763-543-8547"
  },
  {
    "id": "3",
    "nom": "Cothran",
    "prenom": "Greg",
    "email": "Cothran.Greg@hotmail.fr",
    "telephone": "989-218-4214"
  },
  {
    "id": "4",
    "nom": "Voyles",
    "prenom": "Erin",
    "email": "Voyles.Erin@hotmail.fr",
    "telephone": "319-445-1880"
  },
  {
    "id": "5",
    "nom": "Anne",
    "prenom": "Ponce",
    "email": "Anne.Ponce@hotmail.fr",
    "telephone": "856-528-1002"
  },
  {
    "id": "6",
    "nom": "Parks",
    "prenom": "Christopher",
    "email": "Parks.Christopher@hotmail.fr",
    "telephone": "580-369-2078"
  }
]
)

db.Dirigeant.find({"nom":"Voyles"})